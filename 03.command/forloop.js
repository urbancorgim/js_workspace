//반복문
/*
  for(초기값; 조건식; 증감식 ){
    참일때 실행문;
  }

  초가값 ->조건식->참일때 실행문->(증감식 ->조건식->참일때 실행문)반복 ->조건식 거짓일때 종료

  for( index in 객체,배열){
    데이터존재하는 동안 실행할 실행문;
  }

  for( value of 배열,문자열,NodeList){
    데이터존재하는 동안 실행할 실행문;
  }

  초기식;
  while(조건식){
    참일때 실행문;
    증감식;
  }

  초기식 ->조건식 ->실행문 ->증감식->(조건식->실행문->증감식)반복->조건식 false때 종료
*/

//for loop
for(let i=1; i<11 ; i++){
  console.log(i)
}

const cars = ["BMW", "Volvo", "Saab", "Ford", "Fiat", "Audi"];
for(let index=0; index < cars.length; index++){
  console.log(cars[index])
}

//for in loop
const numbers = [45, 4, 9, 16, 25];

let txt = "";
for (let x in numbers) {
  txt += x +":"+numbers[x] +"\t"; 
}
console.log("for in loop object : ",txt);


const person = {fname:"John", lname:"Doe", age:25};

let text = "";
for (let x in person) {   //x key(x)
  text += x+":" + person[x]+"\t";
}
console.log("for in loop array :",text);

//for of loop
//cars - 23line ["BMW", "Volvo", "Saab", "Ford", "Fiat", "Audi"]

let carsText = "";
for (let x of cars) {   // for (let x in cars){
  carsText += x +"\t";  //    carsText += cars[x]+"\t";
}                       // }
console.log(carsText);

//while loop
let loopIndex=1;
while( loopIndex < 11) {
  console.log(loopIndex);
  loopIndex++;
}