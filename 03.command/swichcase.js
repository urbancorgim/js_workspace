//조건문
/*
switch(expression) {           expression    실행
  case 값1 :                      값1        실행문1 실행문2 실행문3
    실행문1;
  case 값2 :                      값2        실행문2 실행문3
    실행문2;
  case 값3 :                      값3        실행문3
    실행문3;
    beak;
  case 값4 :                      값4        실행문4
    실행문4;
    break;
  default :                 값1,2,3,4아닌 값  그외실행문
    그외실행문;
}
*/

let month = 21;
switch(month) {
  case 1:
  case 3:
  case 5:
  case 7:
  case 8:
  case 10:
  case 12: 
        console.log(month,"월은 31일까지 있는 달입니다.");
        break;
  case 2:
        console.log(month,"월은 28일까지 있는 달입니다.");
        break;
  case 4:
  case 6:
  case 9:
  case 11:
        console.log(month,"월은 30일까지 있는 달입니다.");
        break;
  default :
        console.log(month,"월은 없는 달입니다.: month=1~12");
}