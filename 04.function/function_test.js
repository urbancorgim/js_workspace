//1. 함수정의
// function functionName(parameterList){
//   구현
//   return 함수호출하는 곳에 전달할 데이터;
// }

function add(data1, data2){
  let locaVariable = 100; 
  return data1+" + "+data2+ " = "+(data1+data2);
}

//함수호출(사용)
// 변수명 = 함수명(argumentList);  
// argumentList는 함수정의된 parameterList와 개수 일치
let sum = add(10,20);  //data1=10, data2=20
console.log(sum);

//localVariable은 함수 내에서만 사용가능
// console.log(data1, data2, locaVariable); //ReferenceError : undefined
console.log(typeof locaVariable);


//2.Arrow function : 함수이름 없이 일회성으로 사용하는 경우
//(ES6)              arrow function( =>)  사용가능
// 변수명 = function(parameterList){
//        return returnData;
//}
//arrow function 
// 변수명 = (parameterList)=> returnData;
//사용 : 변수명(argumentList);
hello = (val)=>"Hello "+val;

console.log(hello("JavaScript"));


//3.JavaScript - 일급함수 :  함수로 변수로 사용가능
