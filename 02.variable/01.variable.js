//변수 선언하기
var v;  
let l;  // ES6
console.log(" 값 할당하기 전 변수 값 : ", l); //undefined 
y=30
console.log(y)

//값 할당  - 동적타입:값이 할당될때 타입 결정
v=10;
l=20;
console.log("값 할당 후 변수 값 : ", l );
//변수 선언과 값 할당
let z = v+l;
console.log("변수 선언과 동시에 값 할당 : ",z)

//변수명 - 대소문자 구분, Camel Case 
// for = 10;   //keyword사용x
// 1x=10;  //첫글자 문자|_|$로 시작. 숫자로 시작x

z ="100";//SyntaxError Identifier 'z' has already been declared
console.log("같은이름의 변수 선언 : ", z) 


//global variable
var gv =100;
let globalVariable = 100;

{
//local variable
  var gv =50
  let globalVariable = 50

}
console.log("global variable : var  선언한 경우", gv) //local variable 값으로 변경
console.log("global variable : let  선언한 경우", globalVariable) //gloval variable


const PI = 3.141592; //read only - 값 변경없는 상수
//PI=3.14;//TypeError: Assignment to constant variable.

const arr = [10,20,30,40,50]; //배열 상수 - 배열이 참조하는 참조값 변경 못함
console.log("before : ", arr)
arr[0] =1;  //index 1의 값 변경
console.log("after : " ,arr)
// arr =["1","2","3"] //TypeError Assignment to constant variable.

const obj ={"name":"조성훈", "age":29, "major":"경영학"}; //객체 상수
console.log("before : ", obj)
obj["major"] = "클라우드MSA";
console.log("after : " , obj)

// obj ={"name":"송민주", "age":31, "major":"조리외식경영"}//TypeError Assignment to constant variable.


