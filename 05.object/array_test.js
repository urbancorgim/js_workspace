//배열 - 하나의 변수에 여러 데이터를 저장 관리
//배열이름 = [value, value,...]  //new Array(value, value,...);
//value : string, number, null,  true, false, object, array
//Array : length -배열의 길이
//배열접근 : index - 0 ~ length-1,  배열명[index]  IndexError주의
const cars = ["Saab", "Volvo", "BMW"];//new Array("Saab", "Volvo", "BMW");

for(let index=0; index < cars.length; index++){
  console.log(`cars array [${index}] = ${cars[index]}`);
}
for( index in  cars){
  console.log(`forin loop ${index} = ${cars[index]}`);
}
for( car of cars ){
  console.log(`forof loop : ${car}`);
}
//foreach() : 배열 for loop로 처리
cars.forEach(car => console.log(`forecach : ${car}`) )

//Array method
//join(seperator*) : 배열 element seperator로 구분해서 문자열로 리턴
console.log(cars.join("-")); //"Saab-Volvo-BMW"
//배열 element 삭제 pop() : 마지막 element 삭제하고 리턴, empty인 배열 -undefined return 
console.log(`before: ${cars}  cars.pop():${cars.pop()} after:${cars}`);
//배열element 추가 push(elementList) : 배열에 element 추가(append) 후 length return
console.log(`before:${cars} push:${cars.push("KIA", "HYNDAI")} after:${cars}`); 
//배열 삭제 : delete
delete cars[cars.length-1]; //last element 삭제
console.log(`length-1 마지막 index element 삭제 후 ${cars}`);
//splice(startIndex, deleteCount, itemList):배열의 기존 요소를 삭제 또는 교체하거나 새 요소를 삽입하여 배열의 내용을 변경
const months = ['Jan', 'March', 'April', 'June'];
months.splice(1, 0, 'Feb');// 삽입:index 1
console.log(months);//"Jan", "Feb", "March", "April", "June"
months.splice(4, 1, 'May');// 수정:1개 데이터 삭제 후 추가 index 4
console.log(months);//"Jan", "Feb", "March", "April", "May"
months.splice(4,1); //삭제 index 4
console.log(months); ////"Jan", "Feb", "March", "April"
//newArray = array.concat(appendArray) :  배열에 값 추가후 새로운 배열return
//cf) push() - append시 array ref변경x - 기존배열의 length 변경
const array1 = ['a', 'b', 'c'];
const array2 = ['d', 'e', 'f'];
const array3 = array1.concat(array2);
console.log(`array1 concat전의 데이터 유지: ${array1} array3:${array3}`); //기존배열 유지 새로운 배열 ref리턴


//array.slice(start, end) : array의 start ~ end-1까지 새로운 배열로 return
//                          주의사항 - 얕은복사(shallow copy), element가 reference값인 경우 deep copy 조작 필요
const animals = ['ant', 'bison', 'camel', 'duck', 'elephant'];
console.log(animals.slice(2), animals); //원본 배열 animals 변경x
// expected output: Array ["camel", "duck", "elephant"]
console.log(animals.slice(2, 4), animals);
// expected output: Array ["camel", "duck"]
console.log(animals.slice(1, 5), animals);
// expected output: Array ["bison", "camel", "duck", "elephant"]
const newAnimals = animals.slice(); //array copy
newAnimals[0] = "dog";//newAnimals 변경
console.log(newAnimals, animals);  //animals 원본 유지