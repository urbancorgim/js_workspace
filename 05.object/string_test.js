let txt = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
//new String(txt); properties:length 문자열길이
console.log(`${txt}:문자열길이 ${txt.length}` ) 

//1. String methods
//slice(indexstart, indexend)
//substring(indexstart, indexend)
let str = "Apple, Banana, Kiwi";

//index:0부터  7번째~13-1까지 리턴
console.log(`slice : ${str.slice(7, 13)}` )//Banana
//index:0부터  7번째~13-1까지 리턴
console.log(`substring : ${str.substring(7,13)}`) //Banana

//slice vs substring  : indextstart > indexend
console.log(`slice : ${str.slice(13, 7)}` )//""  
console.log(`substring : ${str.substring(13,7)}`) //Banana

//2.String search
//indexOf(searchvalue) : 검색한 첫번째 index return
//lastIndexOf(searchvalue) : 검색한 마지막번째 index return
str = "Please locate where 'locate' occurs!";
console.log(`${str} indexOf locate검색 : " ${str.indexOf("locate")}`);    // Returns 7
console.log(`${str} lastIndexOf locate검색 : " ${str.lastIndexOf("locate")}`); //Retruns 21
//정규표현식 검색
//str.search(regxp) : 정규표현식으로 검색 index나 -1 return
//str.match(regxp) :정규표현식 검색 배열또는 null return    //new RegExp(regxp).exec(string)
const paragraph = 'The quick brown fox jumps over the lazy dog. If the dog barked, was it really lazy?';
// any character that is not a word character or whitespace
const regex = /[^\w\s]/g;
console.log(paragraph.search(regex)); //43
console.log(paragraph[paragraph.search(regex)]);//.
console.log(paragraph.match(regex)); //[ '.', ',', '?' ]
//const regexObject = new RegExp(/[^\w\s]/,'g');
results = regex.exec(paragraph);  //패턴 검색한 첫번째 데이터 정보 배열 또는 null return
console.log(typeof results, results instanceof Array);
console.log( regex.test(paragraph)); //패턴 검색한 데이터 존재여부 true 또는 false return


//문자열 포함여부 검색
//str.include(searchValue)  - true || false return
const sentence = 'The quick brown fox jumps over the lazy dog.';
const word = 'fox'; 
//삼항연산자  조건식 : 참일때값 ? 거짖일때값
//Template Literal ``(back ticks) - \n, '', "", ${javascript변수},${javascript표현식} 
console.log(`The word "${word}" ${sentence.includes(word) ? 'is' : 'is not'} in the sentence`); 
