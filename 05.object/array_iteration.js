//Array Iteration
//*foreach(callback(element, index, array)) : 반복을 통한 데이터 검색
const array1 = [10,20,30];
array1.forEach((element,index )=> { console.log(`array1[${index}]=${element}`); });
//for(element of array1) console.log(element);

//*map(callback(element, index, array)) : 반복을 통해 데이터처리 새로운 배열 리턴
const array2 = array1.map( element =>  element*2 ) ;
console.log(`array1 :${array1}  array2:${array2}`);

//*filter(callback(element, index, array)) : 반복을 통해 조건에 맞는 데이터 새로운 배열 리턴
const array3 = array1.filter(element => element > 15);
console.log(`array1 :${array1}  array3:${array3}`); 

//*reduce(callback(accumulator, element, index, array),initalvalue) : 반복을 통해 데이터처리한 accumulator string으로 리턴
//initial value - 100
const array4 = array1.reduce(
  ( accumulator, currentValue ) => accumulator + currentValue,
  100
);//160
console.log(`reduce reault : ${array4}`, typeof array4); 