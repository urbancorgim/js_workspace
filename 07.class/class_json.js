var Car ={
  name:"ford",
  age :14,
  info : function(){
    return this.name +" : "+ this.age;
  },
  get info2(){
    return this.name+" : "+ this.age;
  }
}

console.log(Car.info()); //info함수 호출
console.log(Car.info);   //info 키의 값 함수리턴 
console.log(Car.info2);  //get info호출